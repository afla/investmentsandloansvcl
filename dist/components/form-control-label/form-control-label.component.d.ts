export declare class FormControlLabelComponent {
    disabled: boolean;
    requiredIndicatorCharacter: string;
    label: string;
    subLabel: string;
    prepend: boolean;
    wrapping: boolean;
    required: boolean;
    requiredIndLabel: string;
}
