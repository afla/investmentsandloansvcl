import { ElementRef } from '@angular/core';
export declare class IcogramComponent {
    private elRef;
    label: string;
    flexLabel: boolean;
    prepIcon: string;
    appIcon: string;
    prepIconSrc: string;
    appIconSrc: string;
    constructor(elRef: ElementRef);
}
