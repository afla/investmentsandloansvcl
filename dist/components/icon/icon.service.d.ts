export declare class IconService {
    defaultNameResolver(icon: string): string;
    lookup(icon: string): any;
}
