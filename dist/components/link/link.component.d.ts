export declare class LinkComponent {
    href: string;
    label: string;
    title: string;
    scheme: string;
    prepIcon: string;
    appIcon: string;
    disabled: boolean;
    readonly attrHref: string;
}
