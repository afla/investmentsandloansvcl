import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {RepositoryService} from "../../services/repository.service";
import {InvestmentModel} from "../../model/investment.model";

@Component({
  selector: 'investments-list-component',
  templateUrl: 'investments-list.component.html'//,
  // styleUrls: ['investments-list.component.css']
})
export class InvestmentsListComponent implements OnInit {

  public selectedInvestment;

  @Output('investmentSelected') investmentSelectedEventEmitter = new EventEmitter();

  @Input('selectedInvestment') set SelectedInvestmentDirective(bindingSource: InvestmentModel) {
    if (bindingSource) {
      this.selectedInvestment = bindingSource;
    }
  }

  constructor(public repositoryService: RepositoryService) { }

  ngOnInit() {
  }

  selected(investment) {
    this.selectedInvestment = investment;
    this.investmentSelectedEventEmitter.emit(investment);
  }

  removeInvestment(investment: InvestmentModel) {
    this.repositoryService.removeInvestment(investment.id);
    this.selectedInvestment = null;
    this.investmentSelectedEventEmitter.emit(null);

  }
}
