import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {NgForm} from "@angular/forms";
import {InvestmentModel} from "../../model/investment.model";
import {RepositoryService} from "../../services/repository.service";

@Component({
  selector: 'investment-edit-component',
  templateUrl: 'investment-edit.component.html'//,
  // styleUrls: ['investment-edit.component.css']
})
export class InvestmentEditComponent implements OnInit {

  public originalInvestment: InvestmentModel = new InvestmentModel();

  @Input('originalInvestment') set InvestmentDirective(bindingSource: InvestmentModel) {
    if (bindingSource) {
      this.originalInvestment.id = bindingSource.id;
      this.originalInvestment.investedAmount = bindingSource.investedAmount;
      this.originalInvestment.returnedAmount = bindingSource.returnedAmount;
      this.originalInvestment.investedDate = bindingSource.investedDate;
      this.originalInvestment.returnedDate = bindingSource.returnedDate;
      this.originalInvestment.clientId = bindingSource.clientId;
    } else {
      this.originalInvestment = new InvestmentModel();
    }
  }

  @Output('updateInvestment') updateInvestmentEventEmitter = new EventEmitter();
  @Output('createInvestment') createInvestmentEventEmitter = new EventEmitter();
  @Output('investmentRemoved') removeInvestmentEventEmitter = new EventEmitter();

  constructor(private repositoryService: RepositoryService) { }

  ngOnInit() { }

  updateInvestment(formDetails: NgForm) {
    this.updateInvestmentEventEmitter.emit(formDetails.value.investment);
  }

  createInvestment(formDetails: NgForm) {
    this.createInvestmentEventEmitter.emit(formDetails.value.investment);
  }

  removeInvestment(formDetails: NgForm) {
    this.removeInvestmentEventEmitter.emit(formDetails.value.investment);
    this.repositoryService.removeInvestment(formDetails.value.investment.id);
    this.originalInvestment = new InvestmentModel();
  }
}
