import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {RepositoryService} from "../../services/repository.service";
import {LoanModel} from "../../model/loan.model";

@Component({
  selector: 'loans-list-component',
  templateUrl: 'loans-list.component.html'//,
  // styleUrls: ['loans-list.component.css']
})
export class LoansListComponent implements OnInit {

  public selectedLoan;

  @Output('loanSelected') loanSelectedEventEmitter = new EventEmitter();

  @Input('selectedLoan') set SelectedLoanDirective(bindingSource: LoanModel) {
    if (bindingSource) {
      this.selectedLoan = bindingSource;
    }
  }

  constructor(public repositoryService: RepositoryService) { }

  ngOnInit() {
  }

  selected(loan) {
    this.selectedLoan = loan;
    this.loanSelectedEventEmitter.emit(loan);
  }

  removeLoan(loan: LoanModel) {
    this.repositoryService.removeLoan(loan.id);
    this.selectedLoan = null;
    this.loanSelectedEventEmitter.emit(null);

  }
}
