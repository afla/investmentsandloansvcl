import {Component} from "@angular/core";
import {LoanModel} from "../../model/loan.model";
import {RepositoryService} from "../../services/repository.service";

@Component({
    selector: 'loans-crud-component',
    templateUrl: 'loans-crud.component.html'
})
export class LoansCrudComponent {
    public selectedLoan;

    constructor(private repositoryService: RepositoryService) {
    }

    updateLoan(loan: LoanModel) {
      this.selectedLoan.amount = loan.amount;
      this.selectedLoan.repayAmount = loan.repayAmount;
      this.selectedLoan.agreementRepayDate = loan.agreementRepayDate;
      this.selectedLoan.actualRepayDate = loan.actualRepayDate;
      this.selectedLoan.startDate = loan.startDate;
      this.selectedLoan.repayed = loan.repayed;
      this.selectedLoan.clientId = loan.clientId;
    }

    createLoan(loan: LoanModel) {
      this.repositoryService.createLoan(loan, this);
    }

}