import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nav-bar-component',
  templateUrl: 'nav-bar.component.html'
  // styles: ['nav { background-color: #dcdcdc; padding: 0px; margin: 0px; }']
})
export class NavBarComponent implements OnInit {

  navbarItems = [
    {
      label: 'Clients CRUD',
      route: ['/clients-crud'],
      selected: true,
      active: true
    },
    {
      label: 'Investments CRUD',
      route: ['/investments-crud'],
      active: true
    },
    {
      label: 'Loans CRUD',
      route: ['/loans-crud'],
      active: true
    }
  ];

  // const routes: Routes = [
  //   {path: '', redirectTo: 'clients-crud', pathMatch: 'full'},
  //   {path: 'clients-crud', component: ClientsCrudComponent},
  //   {path: 'investments-crud', component: InvestmentsCrudComponent},
  //   {path: 'loans-crud', component: LoansCrudComponent}
  // ];


  constructor() { }

  ngOnInit() {
  }

}
