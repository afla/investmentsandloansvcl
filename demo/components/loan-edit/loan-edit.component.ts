import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {NgForm} from "@angular/forms";
import {LoanModel} from "../../model/loan.model";
import {RepositoryService} from "../../services/repository.service";

@Component({
  selector: 'loan-edit-component',
  templateUrl: 'loan-edit.component.html'//,
  // styleUrls: ['loan-edit.component.css']
})
export class LoanEditComponent implements OnInit {

  public originalLoan: LoanModel = new LoanModel();

  @Input('originalLoan') set LoanDirective(bindingSource: LoanModel) {
    if (bindingSource) {
      this.originalLoan.id = bindingSource.id;
      this.originalLoan.amount = bindingSource.amount;
      this.originalLoan.repayAmount = bindingSource.repayAmount;
      this.originalLoan.agreementRepayDate = bindingSource.agreementRepayDate;
      this.originalLoan.actualRepayDate = bindingSource.actualRepayDate;
      this.originalLoan.startDate = bindingSource.startDate;
      this.originalLoan.repayed = bindingSource.repayed;
      this.originalLoan.clientId = bindingSource.clientId;
    } else {
      this.originalLoan = new LoanModel();
    }
  }

  @Output('updateLoan') updateLoanEventEmitter = new EventEmitter();
  @Output('createLoan') createLoanEventEmitter = new EventEmitter();
  @Output('loanRemoved') removeLoanEventEmitter = new EventEmitter();

  constructor(private repositoryService: RepositoryService) { }

  ngOnInit() { }

  updateLoan(formDetails: NgForm) {
    this.updateLoanEventEmitter.emit(formDetails.value.loan);
  }

  createLoan(formDetails: NgForm) {
    this.createLoanEventEmitter.emit(formDetails.value.loan);
  }

  removeLoan(formDetails: NgForm) {
    this.removeLoanEventEmitter.emit(formDetails.value.loan);
    this.repositoryService.removeLoan(formDetails.value.loan.id);
    this.originalLoan = new LoanModel();
  }
}
