import { Component } from '@angular/core';

@Component({
  templateUrl: 'tab-nav.component.html'
})
export class TabNavComponent {
  tabIndex: number = 0;
}
