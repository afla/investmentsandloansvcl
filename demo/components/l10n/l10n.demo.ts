export default {
  name: 'L10n',
  path: 'l10n',
  category: 'Other',
  tabs: {
    'README.md': require("!raw!../../../src/l10n/README.md"),
  }
};
