import {Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import {NgForm} from "@angular/forms";
import {ClientModel} from "../../model/client.model";
import {RepositoryService} from "../../services/repository.service";

@Component({
  selector: 'client-edit-component',
  templateUrl: 'client-edit.component.html'//,
  // styleUrls: ['client-edit.component.css']
})
export class ClientEditComponent implements OnInit {

  public originalClient: ClientModel = new ClientModel();

  @Input('originalClient') set ClientDirective(bindingSource: ClientModel) {
    if (bindingSource) {
      this.originalClient.id = bindingSource.id;
      this.originalClient.name = bindingSource.name;
      this.originalClient.phone = bindingSource.phone;
    } else {
      this.originalClient = new ClientModel();
    }
  }

  @Output('updateClient') updateClientEventEmitter = new EventEmitter();
  @Output('createClient') createClientEventEmitter = new EventEmitter();
  @Output('clientRemoved') removeClientEventEmitter = new EventEmitter();

  constructor(private repositoryService: RepositoryService) { }

  ngOnInit() { }

  updateClient(formDetails: NgForm) {
    this.updateClientEventEmitter.emit(formDetails.value.client);
  }

  createClient(formDetails: NgForm) {
    this.createClientEventEmitter.emit(formDetails.value.client);
  }

  removeClient(formDetails: NgForm) {
    this.removeClientEventEmitter.emit(formDetails.value.client);
    this.repositoryService.removeClient(formDetails.value.client.id);
    this.originalClient = new ClientModel();
  }
}
