import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {RepositoryService} from "../../services/repository.service";
import {ClientModel} from "../../model/client.model";

@Component({
  selector: 'clients-list-component',
  templateUrl: 'clients-list.component.html'//,
  // styleUrls: ['clients-list.component.css']
})
export class ClientsListComponent implements OnInit {

  public selectedClient: ClientModel;

  @Output('clientSelected') clientSelectedEventEmitter = new EventEmitter();

  @Input('selectedClient') set SelectedClientDirective(bindingSource: ClientModel) {
    if (bindingSource) {
      this.selectedClient = bindingSource;
    }
  }

  constructor(public repositoryService: RepositoryService) { }

  ngOnInit() {
  }

  selected(client) {
    this.selectedClient = client;
    this.clientSelectedEventEmitter.emit(client);
  }

  removeClient(client: ClientModel) {
    this.repositoryService.removeClient(client.id);
    this.selectedClient = null;
    this.clientSelectedEventEmitter.emit(null);

  }
}
