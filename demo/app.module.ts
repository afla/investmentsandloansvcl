import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { VCLModule } from '../src/index';
import { L10nModule, L10nStaticLoaderService } from '../src/l10n/l10n.module';

import { AppComponent } from './app.component';
import { HomeComponent } from "./components/home/home.component";
import { MarkdownComponent } from "./components/markdown/markdown.component";
import { DemoComponent, DemoContentComponent } from "./components/demo/demo.component";
import { DEMOS } from "./demos";

// import { routing, appRoutingProviders } from './app.routes';
import {HttpModule} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';


import {RepositoryService} from './services/repository.service';
import {NavBarComponent} from './components/nav-bar/nav-bar.component';
import {ClientsCrudComponent} from './components/clients-crud/clients-crud.component';
import {InvestmentsCrudComponent} from './components/investments-crud/investments-crud.component';
import {LoansCrudComponent} from './components/loans-crud/loans-crud.component';
import {ClientsListComponent} from './components/clients-list/clients-list.component';
import {InvestmentsListComponent} from './components/investments-list/investments-list.component';
import {LoansListComponent} from './components/loans-list/loans-list.component';
import {ClientEditComponent} from './components/client-edit/client-edit.component'
import {LoanEditComponent} from './components/loan-edit/loan-edit.component'
import {InvestmentEditComponent} from './components/investment-edit/investment-edit.component'

const routes: Routes = [
  {path: '', redirectTo: 'clients-crud', pathMatch: 'full'},
  {path: 'clients-crud', component: ClientsCrudComponent},
  {path: 'investments-crud', component: InvestmentsCrudComponent},
  {path: 'loans-crud', component: LoansCrudComponent}
];

@NgModule({
  providers: [
    // appRoutingProviders,
    RepositoryService
  ],
  imports: [
    FormsModule,
    BrowserModule,
    // routing,
    VCLModule,
    L10nModule.forRoot({
      config: {
        // locale: 'de-de'
      },
      loader: L10nStaticLoaderService,
      loaderConfig: {

      }
    }),
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    DemoComponent,
    MarkdownComponent,
    DemoContentComponent,
    ...(DEMOS.map(dc => Object.keys(dc.tabs).map(key => dc.tabs[key]).filter(o => typeof o === 'function'))),
    NavBarComponent,
    ClientsCrudComponent,
    InvestmentsCrudComponent,
    LoansCrudComponent,
    ClientsListComponent,
    ClientEditComponent,
    InvestmentsListComponent,
    InvestmentEditComponent,
    LoansListComponent,
    LoanEditComponent
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule {
}
