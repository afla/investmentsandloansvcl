import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Rx";
import {ClientModel} from "../model/client.model";
import {InvestmentModel} from "../model/investment.model";
import {ClientsCrudComponent} from "../components/clients-crud/clients-crud.component";
import {InvestmentsCrudComponent} from "../components/investments-crud/investments-crud.component";
import {LoanModel} from "../model/loan.model";
import {LoansCrudComponent} from "../components/loans-crud/loans-crud.component";
import {ClientsListComponent} from "../components/clients-list/clients-list.component";

@Injectable()
export class RepositoryService {
    clients: ClientModel[];
    investments: InvestmentModel[];
    loans: LoanModel[];

    constructor(private http: Http) {
        this.http.get('clients.json')
            .subscribe((res: Response) => { this.clients = res.json(); });

        this.http.get('investments.json')
            .subscribe((res: Response) => { this.investments = res.json(); });

        this.http.get('loans.json')
            .subscribe((res: Response) => { this.loans = res.json(); });
    }

    createClient(client: ClientModel, component: ClientsCrudComponent) {
        Observable.from(this.clients)
            .map(client => client.id)
            .max()
            .subscribe(maxId => {
                let newClient = new ClientModel();
                newClient.id = maxId + 1;
                newClient.name = client.name;
                newClient.phone = client.phone;
                this.clients.push(newClient);
                component.selectedClient = newClient;
            });
    }

    createInvestment(investment: InvestmentModel, component: InvestmentsCrudComponent) {
        Observable.from(this.investments)
            .map(investment => investment.id)
            .max()
            .subscribe(maxId => {
                let newInvestment = new InvestmentModel();
                newInvestment.id = maxId + 1;
                newInvestment.investedAmount = investment.investedAmount;
                newInvestment.returnedAmount = investment.returnedAmount;
                newInvestment.investedDate = investment.investedDate;
                newInvestment.returnedDate = investment.returnedDate;
                newInvestment.clientId = investment.clientId;
                this.investments.push(newInvestment);
                component.selectedInvestment = newInvestment;
            });
    }

    createLoan(loan: LoanModel, component: LoansCrudComponent) {
        Observable.from(this.loans)
            .map(loan => loan.id)
            .max()
            .subscribe(maxId => {
                let newLoan = new LoanModel();
                newLoan.id = maxId + 1;
                newLoan.amount = loan.amount;
                newLoan.repayAmount = loan.repayAmount;
                newLoan.agreementRepayDate = loan.agreementRepayDate;
                newLoan.actualRepayDate = loan.actualRepayDate;
                newLoan.startDate = loan.startDate;
                newLoan.repayed = loan.repayed;
                newLoan.clientId = loan.clientId;
                this.loans.push(newLoan);
                component.selectedLoan = newLoan;
            });
    }

    removeClient(clientId) {
        Observable.from(this.clients)
            .find(client => client.id === clientId)
            .subscribe(client => {
                this.clients.splice(this.clients.indexOf(client), 1);
            });
    }

    removeInvestment(investmentId) {
        Observable.from(this.investments)
            .find(investment => investment.id === investmentId)
            .subscribe(investment => {
                this.investments.splice(this.investments.indexOf(investment), 1);
            });
    }

    removeLoan(loanId) {
        Observable.from(this.loans)
            .find(loan => loan.id === loanId)
            .subscribe(loan => {
                this.loans.splice(this.loans.indexOf(loan), 1);
            });
    }

    getClientDebtSum (client: ClientModel) :Observable<number> {
        if (!this.loans) {
            return Observable.of(0);
        }
        return Observable.from(this.loans)
            .filter(loan => loan.clientId === client.id && loan.repayed === false)
            .map((loan) :number => <number>((<LoanModel>loan).repayAmount))
            .reduce((sum: number, i: number) :number => sum - i, <number>0);
    }

    getClientInvestmentSum (client: ClientModel) :Observable<number> {
        if (!this.investments) {
            return Observable.of(0);
        }
        return Observable.from(this.investments)
            .filter((investment: InvestmentModel) => investment.clientId === client.id && !investment.returnedAmount)
            .map((investment): number => <number>((<InvestmentModel>investment).investedAmount))
            .reduce((sum: number, i: number): number => sum + i, <number>0);
    }

    getBiggestDebtorId () : Observable<number> {
        if (!this.loans || !this.clients) {
            return Observable.of(0);
        }

        return Observable.from(this.loans)
            .filter(loan => loan.repayed === false)
            .groupBy(loan => loan.clientId)
            .flatMap(loansGroupObservable => loansGroupObservable
                .reduce((debtSumStructure, loan) => {
                    return {
                        clientId: loan.clientId,
                        debtSum: debtSumStructure.debtSum + loan.repayAmount
                    };
                }, {clientId: null, debtSum: 0})
            )
            .max((debtSumLeftStructure, debtSumRightStructure) => debtSumLeftStructure.debtSum - debtSumRightStructure.debtSum)
            .map(debtSumStructure => debtSumStructure.clientId);
    }

    getBiggestInvestorId () : Observable<number> {
        if (!this.investments || !this.clients) {
            return Observable.of(0);
        }

        return Observable.from(this.investments)
            .filter(investment => investment.returnedAmount === 0.00)
            .groupBy(investment => investment.clientId)
            .flatMap(investmentsGroupObservable => investmentsGroupObservable
                .reduce((investmentSumStructure, investment) => {
                    return {
                        clientId: investment.clientId,
                        investmentSum: investmentSumStructure.investmentSum + investment.investedAmount
                    };
                }, {clientId: null, investmentSum: 0})
            )
            .max((investmentSumLeftStructure, investmentSumRightStructure) => investmentSumLeftStructure.investmentSum - investmentSumRightStructure.investmentSum)
            .map(investmentSumStructure => investmentSumStructure.clientId);
    }
}
