export class LoanModel {
  id: number;
  amount: number;
  repayAmount: number;
  agreementRepayDate: Date;
  actualRepayDate: Date;
  startDate: Date;
  repayed: boolean = false;
  clientId: number;
}