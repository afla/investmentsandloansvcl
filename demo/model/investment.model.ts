export class InvestmentModel {
  id: number;
  investedAmount: number;
  returnedAmount: number;
  investedDate: Date;
  returnedDate: Date = null;
  clientId: number;
}