import { Component } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { RepositoryService } from "./services/repository.service";

@Component({
  selector: 'app',
  templateUrl: './app.component.html'
})
export class AppComponent {
  showEmptySideClumns: boolean = true;

  constructor (public repositoryService: RepositoryService) {
    Observable.fromEvent(window, 'resize')
      .map(() => document.documentElement.clientWidth)
      .subscribe(width => {
        this.showEmptySideClumns = width > 1000;
      });
  }
}
