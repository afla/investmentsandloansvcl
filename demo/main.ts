import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { platformBrowser } from '@angular/platform-browser';
import { AppModule } from './app.module';
import {enableProdMode} from "@angular/core";
// import { AppModuleNgFactory } from '../tmp/demo/app.module.ngfactory';

// enableProdMode()

platformBrowserDynamic().bootstrapModule(AppModule);
// platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
