import { NgModule } from '@angular/core';
import { InputComponent }   from './input.component';

@NgModule({
  imports: [],
  exports: [InputComponent],
  declarations: [InputComponent],
  providers: [],
})
export class VCLInputModule { }

