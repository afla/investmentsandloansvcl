# vcl-input

Enhanced text input

## Usage:

 ```html
<input vcl-input [(ngModel)]="data1">
<input vcl-input [(ngModel)]="data2" selectAllOnFocus=true>
<input vcl-input [(typedValue)]="data3" valueType="number">
```

### API 

#### Properties:

| Name                | Type        | Default            | Description
| ------------        | ----------- | ------------------ |--------------
| `selectAllOnFocus`  | boolean     | false              | Selects 
| `typedValue` *(1)*  | any         |                    | The current value of the input element. Type is converted as specified in `valueType`  
| `valueType`         | string      | string             | `string` or `number`. Type to use in `typedValue`

*(1) Supports Two-way binding*
