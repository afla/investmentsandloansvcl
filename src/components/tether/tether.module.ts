import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TetherComponent } from './tether.component';

@NgModule({
  imports: [CommonModule],
  exports: [TetherComponent],
  declarations: [TetherComponent]
})
export class VCLTetherModule { }
